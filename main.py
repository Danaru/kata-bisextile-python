from bisextile_checker import BisextileChecker

def main():
    yearList = range(1900,2101)
    results = []
    for y in yearList:
        if(BisextileChecker.is_bisextile(y)):
            results.append(y)
    print(results)



if __name__ == "__main__":
    # execute only if run as a script
    main()