import unittest
from bisextile_checker import BisextileChecker

class BisextileTest(unittest.TestCase):

    def test_should_not_be_bisextile_with_input_1999(self):
        actual = BisextileChecker.is_bisextile(1999)
        self.assertFalse(actual, "Should be false")
    
    def test_should_be_bisextile_with_input_multiple_of_400(self):
        actual = BisextileChecker.is_bisextile(2400)
        self.assertTrue(actual)

    def test_should_be_bisextile_with_input_multiple_of_4_and_not_multiple_of_100(self):
        actual = BisextileChecker.is_bisextile(2004)
        self.assertTrue(actual)

    def test_should_be_bisextile_whith_input_2000_as_string(self):
        with self.assertRaises(TypeError):
            BisextileChecker.is_bisextile("2000")